SimpleForm.setup do |config|

  config.wrappers :material, tag: "div", class: "mdl-textfield mdl-js-textfield mdl-textfield--floating-label", error_class: "is-invalid" do |b|
    b.use :html5
    b.use :placeholder
    b.optional :maxlength
    b.optional :readonly
    b.optional :pattern

    b.use :input, class: "mdl-textfield__input"
    b.use :label, class: "mdl-textfield__label"

    b.use :hint,  wrap_with: { tag: :span, class: "hint" }
    b.use :error, wrap_with: { tag: :span, class: "mdl-textfield__error" }
  end

  config.wrappers :material_checkbox, :tag => "label", :class => "mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" do |b|
    b.use :html5
    b.use :input, class: "mdl-checkbox__input"
    b.use :label_text, :wrap_with => { tag: "span", class: "mdl-checkbox__label" }
  end

  config.wrappers :material_radio_buttons, :tag => "label", :class => "mdl-radio mdl-js-radio mdl-js-ripple-effect" do |b|
    b.use :html5
    b.use :input, class: "mdl-radio__button"
    b.use :label_text, :wrap_with => { tag: "span", class: "mdl-radio__label" }
  end

  config.wrappers :material_switch, :tag => "label", :class => "mdl-switch mdl-js-switch mdl-js-ripple-effect" do |b|
    b.use :html5
    b.use :input, class: "mdl-switch__input"
    b.use :label_text, :wrap_with => { tag: "span", class: "mdl-switch__label" }
  end

  config.default_wrapper = :material

  config.boolean_style = :inline
  #config.button_class = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored"
  config.button_class = "mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored"
  config.error_notification_tag = :div
  config.error_notification_class = "error_notification"
  config.browser_validations = false
  config.boolean_label_class = "checkbox"

  config.wrapper_mappings = {
    boolean: :material_switch,
    check_boxes: :material_checkbox,
    radio_buttons: :material_radio_buttons,
    # file: :vertical_file_input,
    # datetime: :multi_select,
    # date: :multi_select,
    # time: :multi_select
  }

end