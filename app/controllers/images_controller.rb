class ImagesController < ApplicationController

	def index
		@image = Image.all
	end

	def new
		@image = Image.new
	end

	def create
		@image = Image.new(image_params)
		@image.computer = request.ip

		if @image.save
			redirect_to root_path
		else
			render :new
		end
	end

	def destroy
		@image = Image.find params[:id]
		@image.destroy

		redirect_to root_path
	end

	private
	def image_params
		params.require(:image).permit(:description, :link, :computer);
	end

end