class Image < ApplicationRecord

	validates :description, :link, presence: true

end