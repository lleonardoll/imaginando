// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require sweetalert

$(function(){

  $.rails.allowAction = function(link) {

    if (!link.attr('data-confirm')) {
      return true;
    }

    showConfirm(link);
    return false;
  };

  $.rails.confirmed = function(link) {
    link.removeAttr('data-confirm');
    return link.trigger('click.rails');
  };

  function showConfirm(link){
    swal({
      title: link.attr('data-confirm'),
      text: link.attr('data-text'),
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#85bb65',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Cancelar',
      closeOnConfirm: false
    },
    function(){
      $.rails.confirmed(link);
    });
  }

})
